alter table update_log_like
    drop key create_id_update_log_id;

create table roadmap_group
(
    id          varchar(32)                                       not null
        primary key,

    is_delete   tinyint(1) unsigned default 0                     not null comment '逻辑删除',
    create_id   varchar(32)         default ''                    not null comment '创建人',
    create_time datetime            default '1970-01-01 00:00:00' not null comment '创建时间',
    update_id   varchar(32)         default ''                    not null comment '更新人',
    update_time datetime            default '1970-01-01 00:00:00' not null comment '更新时间',

    name        varchar(32)         default ''                    not null comment '路线图分组名称',
    color       varchar(6)          default ''                    not null comment '颜色，十六进制'

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_swedish_ci comment '路线图分组';


create table roadmap_tag
(
    id          varchar(32)                                       not null
        primary key,

    is_delete   tinyint(1) unsigned default 0                     not null comment '逻辑删除',
    create_id   varchar(32)         default ''                    not null comment '创建人',
    create_time datetime            default '1970-01-01 00:00:00' not null comment '创建时间',

    name        varchar(32)         default ''                    not null comment '标签名称'

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_swedish_ci comment '路线图标签';

create table roadmap
(
    id           varchar(32)                                       not null
        primary key,

    is_delete    tinyint(1) unsigned default 0                     not null comment '逻辑删除',
    create_id    varchar(32)         default ''                    not null comment '创建人',
    create_time  datetime            default '1970-01-01 00:00:00' not null comment '创建时间',
    update_id    varchar(32)         default ''                    not null comment '更新人',
    update_time  datetime            default '1970-01-01 00:00:00' not null comment '更新时间',

    name         varchar(32)         default ''                    not null comment '需求名称',
    priority     tinyint unsigned    default 0                     not null comment '优先级',

    is_top       tinyint(1) unsigned default 0                     not null comment '是否置顶',
    is_complete  tinyint(1) unsigned default 0                     not null comment '是否完成',
    is_abandon   tinyint(1) unsigned default 0                     not null comment '是否放弃',

    view_count   int unsigned        default 0                     not null comment '浏览量',
    reply_count  int unsigned        default 0                     not null comment '回复量',
    like_count   int unsigned        default 0                     not null comment '点赞量',
    unlike_count int unsigned        default 0                     not null comment '踩量'

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_swedish_ci comment '路线图';

create table roadmap_tags
(
    id          varchar(32)                                       not null
        primary key,

    is_delete   tinyint(1) unsigned default 0                     not null comment '逻辑删除',
    create_id   varchar(32)         default ''                    not null comment '创建人',
    create_time datetime            default '1970-01-01 00:00:00' not null comment '创建时间',
    update_id   varchar(32)         default ''                    not null comment '更新人',
    update_time datetime            default '1970-01-01 00:00:00' not null comment '更新时间',

    roadmap_id  varchar(32)         default ''                    not null comment '路线图id',
    tag_id      varchar(32)         default ''                    not null comment '标签id',
    tag_name    varchar(32)         default ''                    not null comment '标签名称'

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_swedish_ci comment '路线图-标签';

create table roadmap_content
(
    id          varchar(32)                                       not null
        primary key,

    is_delete   tinyint(1) unsigned default 0                     not null comment '逻辑删除',
    create_id   varchar(32)         default ''                    not null comment '创建人',
    create_time datetime            default '1970-01-01 00:00:00' not null comment '创建时间',
    update_id   varchar(32)         default ''                    not null comment '更新人',
    update_time datetime            default '1970-01-01 00:00:00' not null comment '更新时间',

    content     longtext comment '内容'

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_swedish_ci comment '路线图内容';
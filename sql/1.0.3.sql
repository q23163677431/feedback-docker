alter table update_log
    add dialog tinyint(1) unsigned default 0 not null comment '弹窗提醒' after publish_date;
alter table update_log_thank
    modify thank_id varchar(32) default '' not null comment '感谢的反馈ID';

alter table update_log_thank
    add user_id varchar(32) default '' not null comment '反馈的创建人，冗余';

alter table update_log_thank
    drop key create_id_update_log_id_thank_id;


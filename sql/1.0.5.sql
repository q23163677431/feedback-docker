create table plugin_tag
(
    id          varchar(32) primary key,

    is_delete   tinyint(1) unsigned not null default 0 comment '逻辑删除',
    create_id   varchar(32)         not null default '' comment '创建人',
    create_time datetime            not null default '1970-01-01 00:00:00' comment '创建时间',
    update_id   varchar(32)         not null default '' comment '更新人',
    update_time datetime            not null default '1970-01-01 00:00:00' comment '更新时间',

    plugin_id   varchar(32)         not null default '' comment '插件ID',

    name        varchar(32)         not null default '' comment '标签名称',
    color       varchar(6)          not null default '' comment '标签颜色',
    sort        int unsigned        not null default 0 comment '排序'
) comment '插件标签';

create table feedback_tag
(
    id            varchar(32) primary key,

    is_delete     tinyint(1) unsigned not null default 0 comment '逻辑删除',
    create_id     varchar(32)         not null default '' comment '创建人',
    create_time   datetime            not null default '1970-01-01 00:00:00' comment '创建时间',
    update_id     varchar(32)         not null default '' comment '更新人',
    update_time   datetime            not null default '1970-01-01 00:00:00' comment '更新时间',

    plugin_id     varchar(32)         not null default '' comment '插件ID',
    feedback_id   varchar(32)         not null default '' comment '反馈ID',
    plugin_tag_id varchar(32)         not null default '' comment '标签ID',

    name          varchar(32)         not null default '' comment '标签名称',
    color         varchar(6)          not null default '' comment '标签颜色'
) comment '反馈标签';
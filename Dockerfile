# 使用 JDK 21 镜像作为基础镜像
FROM openjdk:21

# 前端
COPY ./feedback/dist/ /app/public/
COPY ./feedback-server/docker/nginx.conf /etc/nginx/nginx.conf
VOLUME /app/public

VOLUME /app/resource

# 后端
COPY ./feedback-server/target/feedback-*.jar /app/feedback.jar

ENV MYSQL_URL=""
ENV MYSQL_USERNAME=""
ENV MYSQL_PASSWORD=""
ENV FEEDBACK_WEBSITE=""
ENV FEEDBACK_BEI_AN=""

VOLUME /app/log

# 暴露 Nginx 的默认端口
EXPOSE 8080

# 启动 Nginx 服务
CMD ["java", "-jar", "/app/feedback.jar", "--spring.profiles.active=pro"]